package br.com.contatoautorizador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContatoAutorizadorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContatoAutorizadorApplication.class, args);
	}

}
