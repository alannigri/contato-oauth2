package br.com.contatoautorizador.services;

import br.com.contatoautorizador.models.Contato;
import br.com.contatoautorizador.models.Usuario;
import br.com.contatoautorizador.repositories.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository contatoRepository;

    public Contato criarUsuario(Usuario usuario) {
        Contato contato = new Contato();
        contato.setNomeUsuario(usuario.getNome());
        contato.setLocalDateTime(LocalDateTime.now());
        return contatoRepository.save(contato);
    }

    public Iterable<Contato> consultarUsuario(Usuario usuario) {
        return contatoRepository.findByNomeUsuario(usuario.getNome());
    }

}
