package br.com.contatoautorizador.repositories;

import br.com.contatoautorizador.models.Contato;
import org.springframework.data.repository.CrudRepository;

public interface ContatoRepository extends CrudRepository<Contato, Integer> {
    Iterable<Contato> findByNomeUsuario(String nomeUsuario);
}
