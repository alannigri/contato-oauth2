package br.com.contatoautorizador.controllers;

import br.com.contatoautorizador.models.Contato;
import br.com.contatoautorizador.models.Usuario;
import br.com.contatoautorizador.services.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/contato")
public class ContatoController {

    @Autowired
    private ContatoService contatoService;

    @PostMapping
    public Contato criarContato(@AuthenticationPrincipal Usuario usuario) {
        return contatoService.criarUsuario(usuario);
    }
    //
    @GetMapping
    public Iterable<Contato> buscarContatos(@AuthenticationPrincipal Usuario usuario){
        return contatoService.consultarUsuario(usuario);
    }
}